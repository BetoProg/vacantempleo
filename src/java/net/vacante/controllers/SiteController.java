
package net.vacante.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.vacante.dao.DbConnection;
import net.vacante.dao.VacanteDao;
import net.vacante.model.Vacante;


public class SiteController extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        RequestDispatcher rd;
        DbConnection cn = new DbConnection();
        
        List<Vacante> lista =  VacanteDao.getUltimas();
        request.setAttribute("listaultimas", lista);
        rd = request.getRequestDispatcher("/Home/home.jsp");
        rd.forward(request, response);
                
    }

}
