/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package net.vacante.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.vacante.dao.VacanteDao;
import net.vacante.model.Vacante;

/**
 *
 * @author LYNX
 */
public class BusquedaController extends HttpServlet {

   
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String q = request.getParameter("query");
        List<Vacante> lista = null;
        VacanteDao vd = new VacanteDao();
        lista = vd.getByQuery(q);
        
        RequestDispatcher rd;
        request.setAttribute("vacantes", lista);
        rd = request.getRequestDispatcher("./Vacante/vacantes.jsp");
        rd.forward(request, response);
    }

  

}
