
package net.vacante.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Set;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.vacante.dao.DbConnection;
import net.vacante.dao.VacanteDao;
import net.vacante.model.Vacante;

public class VacanteController extends HttpServlet {

    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException 
    {
       String action = request.getParameter("action");
       
       if(action.equals("ver")){
          this.verDetalle(request,response);
       }else if(action.equals("lista")){
           this.verTodas(request,response);
       }
        
    }
    
    public void verTodas(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException 
    {
        VacanteDao vd = new VacanteDao();
        
        List<Vacante> lista = vd.getAll();
        request.setAttribute("vacantes", lista);
        RequestDispatcher rd;
        rd = request.getRequestDispatcher("./Vacante/vacantes.jsp");
        rd.forward(request, response);
    
    }
    
    public void verDetalle(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException 
    {
        int idVacante = Integer.parseInt(request.getParameter("id"));
        
        Vacante vacante = VacanteDao.getById(idVacante);
        request.setAttribute("vacante", vacante);
        RequestDispatcher rd;
        
        rd = request.getRequestDispatcher("./Vacante/detalle.jsp");
        rd.forward(request, response);
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String nombre = request.getParameter("nombre");
        String descripcion = request.getParameter("descripcion");
        String detalle = request.getParameter("detalle");
        
        Vacante vacante = new Vacante();
        
        vacante.setNombre(nombre);
        vacante.setDescripcion(descripcion);
        vacante.setDetalle(detalle);
        
        //System.out.println(vacante);
        
        VacanteDao vd = new VacanteDao();
        boolean status = vd.insert(vacante);
        String mensaje = "";
        
        if(status){
            mensaje = "La insercion de la vacante se hizo con exito";
        }else{
            mensaje = "No se inserto correctamente vacante";
        }
        
        RequestDispatcher rd;
        request.setAttribute("mensaje", mensaje);
        //Enviamos la respueta a la vista de mensajes
        rd = request.getRequestDispatcher("/mensaje.jsp");
        rd.forward(request, response);

    }

}
