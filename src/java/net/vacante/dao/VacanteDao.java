
package net.vacante.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.vacante.model.Vacante;


public class VacanteDao {
    private static DbConnection cn = new DbConnection();
    
    public VacanteDao() {
        
    }
    
    public boolean insert(Vacante vacante){
        //Metodo de insercion de datos de vacante
        String sql = "insert into vacante values(?,?,?,?,?)";
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        
        try {
            
            PreparedStatement ps = cn.getConnection().prepareStatement(sql);
            ps.setInt(1, vacante.getId());
            ps.setString(2, formato.format(vacante.getFechaPublicacion()));
            ps.setString(3, vacante.getNombre());
            ps.setString(4, vacante.getDescripcion());
            ps.setString(5, vacante.getDetalle());
            ps.executeUpdate();
            
            cn.disconnect();
            return true;
            
        } catch (SQLException ex) {
            Logger.getLogger(VacanteDao.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    
    }
    
    public static List<Vacante> getUltimas(){
        String sql = "select * from vacante order by id desc limit 3";
        try {
            PreparedStatement ps = cn.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Vacante> lista = new LinkedList<>();
            
            
            while(rs.next()){
                Vacante vacante = new Vacante();
                vacante.setId(rs.getInt("id"));
                vacante.setNombre(rs.getString("nombre"));
                vacante.setDetalle(rs.getString("detalle"));
                vacante.setDescripcion(rs.getString("descripcion"));
                vacante.setFechaPublicacion(rs.getDate("fechaPublicacion"));
                
                lista.add(vacante);
            }
            //cn.disconnect();
            return lista;
        } catch (SQLException ex) {
            System.out.println("Error Vacante no trajo resultados " + ex.getMessage());
            return null;
        }
    
    }
    
    public static Vacante getById(int idVacante){
        String sql = "select * from vacante where id=? limit 1";
        try {
            PreparedStatement ps = cn.getConnection().prepareStatement(sql);
            ps.setInt(1, idVacante);
            ResultSet rs = ps.executeQuery();
            Vacante vacante=null;
            
            while(rs.next()){
                vacante = new Vacante();
                vacante.setId(rs.getInt("id"));
                vacante.setNombre(rs.getString("nombre"));
                vacante.setDetalle(rs.getString("detalle"));
                vacante.setDescripcion(rs.getString("descripcion"));
                vacante.setFechaPublicacion(rs.getDate("fechaPublicacion"));
                
            }
            return vacante;
            
        } catch (SQLException ex) {
             System.out.println("Error Vacante no trajo resultados " + ex.getMessage());
             return null;
        }
    
    }
    
    
    public List<Vacante> getAll(){
        String sql = "select * from vacante order by id desc";
        try {
            PreparedStatement ps = cn.getConnection().prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            List<Vacante> lista = new LinkedList<>();
            
            
            while(rs.next()){
                Vacante vacante = new Vacante();
                vacante.setId(rs.getInt("id"));
                vacante.setNombre(rs.getString("nombre"));
                vacante.setDetalle(rs.getString("detalle"));
                vacante.setDescripcion(rs.getString("descripcion"));
                vacante.setFechaPublicacion(rs.getDate("fechaPublicacion"));
                
                lista.add(vacante);
            }
            //cn.disconnect();
            return lista;
        } catch (SQLException ex) {
            System.out.println("Error Vacante no trajo resultados " + ex.getMessage());
            return null;
        }
    
    }
    
    public List<Vacante> getByQuery(String query){
        String sql = "select * from vacante where (descripcion like ? or nombre like ?) order by id desc";
        try {
            PreparedStatement ps = cn.getConnection().prepareStatement(sql);
            ps.setString(1,"%" + query + "%");
            ps.setString(2,"%" + query + "%");
            ResultSet rs = ps.executeQuery();
            List<Vacante> lista = new LinkedList<>();
            
            while(rs.next()){
                Vacante vacante = new Vacante();
                vacante.setId(rs.getInt("id"));
                vacante.setNombre(rs.getString("nombre"));
                vacante.setDetalle(rs.getString("detalle"));
                vacante.setDescripcion(rs.getString("descripcion"));
                vacante.setFechaPublicacion(rs.getDate("fechaPublicacion"));
                
                lista.add(vacante);
            }
            return lista;
            
        } catch (SQLException ex) {
            System.out.println("Error Vacante no trajo resultados getByQuery " + ex.getMessage());
            return null;
        }
    }
}
