package net.vacante.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbConnection {
    
    static String bd = "dbvacantes";
    static String login = "root";
    static String password = "";
    static String url = "jdbc:mysql://localhost/" + bd;
    //Variable de coneccion
    Connection conn = null;

    public DbConnection() {
        try{
            Class.forName("com.mysql.jdbc.Driver");
            //Obtenemos la conexion
            conn = DriverManager.getConnection(url, login, password);
            if(conn!=null){
                System.out.println("Connection database ok");
            }
            
        }
        catch(SQLException e){
            System.out.println("Error: " + e.getMessage());
        }
        catch(ClassNotFoundException e){
            System.out.println("Excepcion driver: " + e.getMessage());
        }  
    }
        
    public Connection getConnection(){
        return conn;
    }

    public void disconnect(){
        if(conn!=null){
            try{
                conn.close();
            }catch(SQLException e){
                System.out.println(e);
            }
        }
    }
}
    
    
