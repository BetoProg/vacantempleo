<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<mt:plantilla titile="Home page">
    <jsp:attribute name="content">   
        <h1>Pagina de Inicio de vacantes</h1>
        <!-- Example row of columns -->
        <div class="row">
            <c:forEach items="${listaultimas}" var="vacante" varStatus="status">
                <div class="col-lg-4">
                    <h2>${vacante.id}</h2>
                    <p class="text-danger text-justify">${vacante.nombre}</p>
                    <p class="text-justify">${vacante.descripcion}</p>
                    <p><a class="btn btn-primary" href="vacante?action=ver&id=${vacante.id}" 
                          role="button">Ver detalles</a></p>
                </div>
            </c:forEach>
        </div>
        <br>
        <div class="row">
            <a href="vacante?action=lista" class="btn btn-success">Ver mas ofertas</a>
        </div>
    </jsp:attribute>
</mt:plantilla>
