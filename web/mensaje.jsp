<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<mt:plantilla titile="mensajes">
    <jsp:attribute name="content">
        <div class="alert alert-primary" role="alert">
            ${mensaje}
        </div>
    </jsp:attribute>
</mt:plantilla>