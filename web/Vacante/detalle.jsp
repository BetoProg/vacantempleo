<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<mt:plantilla titile="Detalle">
    <jsp:attribute name="content">   
        <h1>Detalle de vacante</h1>
        <div class="panel panel-primary">
        <div class="panel-heading">
          <h3 class="panel-title">Numero de la vacante</h3>
        </div>
        <div class="panel-body">
          <h5><b>Vacante</b>: ${vacante.nombre}</h5>
          <h5><b>Publicado</b>: ${vacante.fechaPublicacion}</h5>                             
          <b>Descripción:</b><br>
          <p class="text-justify">${vacante.descripcion}</p>
          <b>Detalles de la vacante</b>:<br>
          ${vacante.detalle}
        </div>
      </div>      
    </jsp:attribute>
</mt:plantilla>