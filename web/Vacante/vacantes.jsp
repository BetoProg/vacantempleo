<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<mt:plantilla titile="Vacantes">
    <jsp:attribute name="content">   
        <h1>Vacantes</h1>
        
      <form method ="post" action="buscar" class="navbar-form navbar-right">
        <div class="form-group">
          <input type="text" name="query" required placeholder="Buscar oferta..." class="form-control">
        </div>        
        <button type="submit" class="btn btn-success">Buscar</button>
      </form>
      <br><br><br>

      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><b>Lista de Vacantes</b></h3>
        </div>
        <div class="panel-body">
          <table class="table table-striped">
            <thead>
              <tr>
                <th class="left">ID</th>
                <th>Vacante</th>
                <th>Publicado</th>                
                <th></th>
              </tr>
            </thead>
            <tbody>
                <c:forEach items="vacantes" var="vacante">
                <tr>
                  <td class="left">${vacante.id}</td>
                  <td>${vacante.nombre}</td>
                  <td>${vacante.fechaPublicacion}</td>
                  <td>
                    <a class="btn btn-default" href="vacante?action=ver&id=${vacante.id}" role="button">Ver Detalles</a>                                                    
                    <a class="btn btn-default" href="#" role="button">Eliminar</a>                         
                  </td>  
                </tr>
                </c:forEach>
            </tbody>           
          </table>
        </div>
      </div>
    </jsp:attribute>
</mt:plantilla>