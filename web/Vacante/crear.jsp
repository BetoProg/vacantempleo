<%@page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="mt" tagdir="/WEB-INF/tags"%>
<mt:plantilla titile="Home page">
    <jsp:attribute name="content">   
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Crear Vacante</h3>
        </div>
        <div class="panel-body">
         <form action="../vacante" method="post">
            <div class="form-group">
              <label for="nombre">Nombre</label>
              <input type="text" class="form-control" name="nombre" required id="nombre" value="" placeholder="Escriba el nombre la vacante">
            </div>                   
            <div class="form-group">
              <label for="descripcion">Descripción</label>
              <textarea class="form-control" name="descripcion" id="descripcion" required rows="3" placeholder="Escribe una descripción de la vacante"></textarea>
            </div>
            <div class="form-group">
              <label for="detalle">Escriba los detalles</label>
              <textarea class="form-control" name="detalle" id="detalle" rows="4" placeholder="Escriba los detalles de la vacante"></textarea>
            </div>
            <button type="submit" class="btn btn-success" >Guardar</button>
          </form>
        </div>
      </div>
    </jsp:attribute>
</mt:plantilla>
